#include "Ctriangle.h"
#include "framework.h"

class Cchat {
  public:
    Cchat(HDC *dc, short px1 = 0, short py1 = 0, short px2 = 0, short py2 = 0,
          short px3 = 0, short py3 = 0);

    void Afficher(); // afficher les caractéristiques de la tête de chat
    void Dessiner(); // dessiner dans la fenêtre
    void Deplacer(short, short); // déplacer la tête de chat
    void Zoomer(float);          // Agrandir ou réduire la tête de chat
    ~Cchat();

  private:
    Ctriangle *tete;
    Ctriangle *oreilleGauche;
    Ctriangle *oreilleDroite;
    Ctriangle *oeilGauche;
    Ctriangle *oeilDroit;
    Ctriangle *nez;
};