#include "Cchat.h"

#include <iostream>

using namespace std;

Cchat::Cchat(HDC *dc, short px1, short py1, short px2, short py2, short px3,
             short py3) {
    // Initialisation de la tête
    tete = new Ctriangle(dc, px1, py1, px2, py2, px3, py3);
    short l = px2 - px1;
    short h = py3 - py1;
    // puis de tous les autres triangles
    oreilleGauche =
        new Ctriangle(dc, px1 + l / 6, py1 - h / 3, px1 + l / 3, py1, px1, py1);
    oreilleDroite = new Ctriangle(dc, px1 + 2 * l / 3 + l / 6, py2 - h / 3, px2,
                                  py2, px2 - l / 3, py2);
    oeilGauche =
        new Ctriangle(dc, px1 + l / 3, py1 + h / 6, px1 + l / 3 + l / 12,
                      py1 + h / 4, px1 + l / 3 - l / 12, py1 + h / 4);
    oeilDroit =
        new Ctriangle(dc, px2 - l / 3, py1 + h / 6, px2 - l / 3 + l / 12,
                      py1 + h / 4, px2 - l / 3 - l / 12, py1 + h / 4);
    nez = new Ctriangle(dc, px1 + l / 3, py1 + h / 2, px2 - l / 3, py1 + h / 2,
                        px1 + l / 2, py1 + 2 * h / 3);
}

void Cchat::Afficher() {
    cout << "Affichage du chat :" << endl;
    cout << "Tete :" << endl;
    tete->Afficher();
    cout << "Oreille gauche :" << endl;
    oreilleGauche->Afficher();
    cout << "Oreille droite :" << endl;
    oreilleDroite->Afficher();
    cout << "Oeil gauche :" << endl;
    oeilGauche->Afficher();
    cout << "Oeil droit :" << endl;
    oeilDroit->Afficher();
    cout << "Nez :" << endl;
    nez->Afficher();
}

void Cchat::Dessiner() {
    tete->Dessiner();
    oreilleGauche->Dessiner();
    oreilleDroite->Dessiner();
    oeilGauche->Dessiner();
    oeilDroit->Dessiner();
    nez->Dessiner();
}

void Cchat::Deplacer(short pX, short pY) {
    this->tete->Deplacer(pX, pY);
    this->nez->Deplacer(pX, pY);
    this->oreilleDroite->Deplacer(pX, pY);
    this->oreilleGauche->Deplacer(pX, pY);
    this->oeilDroit->Deplacer(pX, pY);
    this->oeilGauche->Deplacer(pX, pY);
}

Cchat::~Cchat() {
    delete tete;
    delete oeilDroit;
    delete oeilGauche;
    delete oreilleDroite;
    delete oreilleGauche;
    delete nez;
}