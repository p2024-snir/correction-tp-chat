#pragma once
#include "framework.h"

class Ctriangle {
  public:
    Ctriangle(HDC *pdc = NULL, short px1 = 0, short py1 = 0, short px2 = 0,
              short py2 = 0, short px3 = 0, short py3 = 0);
    // = 0 valeur par défaut des paramètres et avoir un constructeur par défaut
    void Afficher(void);
    void Deplacer(short, short);
    void Dessiner(void);
    // Mutateur de toutes les valeurs
    void setCtriangle(HDC *pdc, short px1, short py1, short px2, short py2,
                      short px3, short py3);
    // Accesseur et mutateur de x1
    short GetX1();
    void  SetX1(short newX1);
    ~Ctriangle();

  private:
    HDC * dc;
    short x1, y1;
    short x2, y2;
    short x3, y3;
}; // NB : ne pas oublier ;
