#include "Ctriangle.h"
#include "framework.h"

#include <iostream>
using namespace std;

// Constructeur : Init des attributs
Ctriangle::Ctriangle(HDC *pdc, short px1, short py1, short px2, short py2,
                     short px3, short py3)
    : dc(pdc), x1(px1), y1(py1), x2(px2), y2(py2), x3(px3), y3(py3) {
    cout << "Passage par le constructeur de triangle." << endl;
}

void Ctriangle::Afficher(void) {
    // Objectif = Afficher en mode texte les caractéristiques du triangle
    cout << "Coordonnees S1 = " << x1 << " et " << y1 << endl;
    cout << "Coordonnees S2 = " << x2 << " et " << y2 << endl;
    cout << "Coordonnees S3 = " << x3 << " et " << y3 << endl;
}

void Ctriangle::Deplacer(short px, short py) {
    x1 = x1 + px;
    y1 = y1 + py;
    x2 = x2 + px;
    y2 = y2 + py;
    x3 = x3 + px;
    y3 = y3 + py;
}

void Ctriangle::Dessiner() {
    // se déplacer au premier point
    MoveToEx(*dc, x1, y1, NULL);
    LineTo(*dc, x2, y2);
    LineTo(*dc, x3, y3);
    LineTo(*dc, x1, y1);
}

void Ctriangle::setCtriangle(HDC *pdc, short px1, short py1, short px2,
                             short py2, short px3, short py3) {
    this->dc = pdc;
    this->x1 = px1;
    this->y1 = py1;
    this->x2 = px2;
    this->y2 = py2;
    this->x3 = px3;
    this->y3 = py3;
}

short Ctriangle::GetX1() { return this->x1; }
void  Ctriangle::SetX1(short newX1) { this->x1 = newX1; }

Ctriangle::~Ctriangle() {
    cout << "Passage par le destructeur de triangle." << endl;
}